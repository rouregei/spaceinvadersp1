package game;

import game.Craft;
import userInterface.ViewController;

public class GameController implements Runnable {
	private int width, heigth;
    private final long DELAY = 10;
    private Craft craft;
	private Thread animator;
	private ViewController viewController;

	
	public GameController (int w, int h, ViewController viewCtl) {
		width = w;
		heigth = h;
		viewController = viewCtl;
	}
	
	public void initGame () {
		craft = new Craft(width, heigth, width/2, heigth/2);
		craft.setVelocityX(1);
	}

    public void start() {
        initGame();
        animator = new Thread(this);
        animator.start();
    }

	
	private void slowerGame(long beforeTime) {
        long timeDiff, sleep;

        timeDiff = System.currentTimeMillis() - beforeTime;
        sleep = DELAY - timeDiff;
        if (sleep < 0) {
            sleep = 2;
        }

        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }

    }
	
	@Override
	public void run() {

        long beforeTime;

        beforeTime = System.currentTimeMillis();

        while (true) {
            //moveElements();
        	craft.move();
            
            //checkCollisions();
            
            //paint
        	viewController.paint();

            slowerGame(beforeTime);
            beforeTime = System.currentTimeMillis();
        }	
     }

	public Craft getCraft() {
		return craft;
	}
}
