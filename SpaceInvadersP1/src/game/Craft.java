package game;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Craft {
	protected int boardHeight;
	protected int boardWidth;

	protected int dx;
	protected int dy;

	protected int height;
	protected int width;

	protected int x;
	protected int y;

	protected Image image;

	public Craft (int bw, int bh, int x, int y) {
		this.x=x;
		this.y=y;
		boardWidth = bw;
		boardHeight = bh;
        setImage("images/craft.png");
	}


	public int getHeight() {
		return height;
	}

	public Image getImage() {
		return image;
	}

	public int getWidth() {
		return width;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}


	public void move() {
		x += dx;
		y += dy;
		
        if (x<0) x=boardHeight;
        else if (x > boardWidth) x=0;
        
        if (y<0) y=boardHeight;
        else if (y > boardHeight) y=0;
	}

	public final void setImage(String imgFile) {
		ImageIcon ii = new ImageIcon(imgFile);
		image = ii.getImage();
		width = image.getWidth(null);
		height = image.getHeight(null);
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
    public void setVelocityX(int dx) {
        this.dx = dx;
    }

    public void setVelocityY(int dy) {
        this.dy = dy;
    }
}
