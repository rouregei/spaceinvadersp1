package userInterface;

import game.Craft;
import game.GameController;

public class ViewController {
	private MainFrame gameGUI;
	private Board board;
	private GameController game;

	public ViewController(MainFrame mainFrame, Board board) {
		this.board = board;
		
		this.board.setViewController(this);
		gameGUI = mainFrame;
		
		game = new GameController(board.getWidth(), board.getHeight(), this);
		game.start();
	}
	
	public void paint() {
		board.repaint();
	}
	
	public Craft getCraft() {
		return game.getCraft();
	}

}
