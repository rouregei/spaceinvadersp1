package userInterface;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.border.CompoundBorder;
import java.awt.Color;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private ViewController viewController;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 825, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		LeftPanel leftPanel = new LeftPanel();
		leftPanel.setBackground(Color.LIGHT_GRAY);
		leftPanel.setBorder(new CompoundBorder());
		leftPanel.setBounds(new Rectangle(5, 5, 137, 552));
		contentPane.add(leftPanel);
		
		Board board = new Board();
		board.setLocation(146, 5);
		board.setBackground(Color.BLACK);
		board.setBorder(new CompoundBorder());
		board.setSize(new Dimension(663, 552));
		contentPane.add(board);
		
		viewController = new ViewController(this,board);
	}

}
