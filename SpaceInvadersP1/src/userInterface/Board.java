package userInterface;

import game.Craft;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;

public class Board extends JPanel {

	private ViewController viewController;
	/**
	 * Create the panel.
	 */
	public Board() {
		setBackground(Color.BLACK);
        setFocusable(true);  //if false doesn't listen key events
        setDoubleBuffered(true);
	}

    public void paint(Graphics g) {
        super.paint(g);
        
        
        Graphics2D g2d = (Graphics2D) g;

        Craft c = viewController.getCraft();
        g2d.drawImage(c.getImage(), c.getX(), c.getY(), this);
        
        Toolkit.getDefaultToolkit().sync();
        g.dispose();

    }

	public void setViewController(ViewController viewCtrl) {
		viewController = viewCtrl;
	}

}
